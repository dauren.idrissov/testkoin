package kz.daurenidrissov.testkoin.model

import androidx.lifecycle.ViewModel

open class Person(val hobby: Hobby, val dreamJob: DreamJob) {
    open fun chill(): String {
        return hobby.findHobby() + " " + dreamJob.findDreamJob()
    }
}

class Person2(hobby: Hobby, dreamJob: DreamJob) : Person(hobby, dreamJob) {
    override fun chill(): String {
        return hobby.findHobby() + " But I don't like my job! "
    }
}

class Hobby {
    fun findHobby(): String {
        return "I love my hobby!"
    }
}

class DreamJob {
    fun findDreamJob(): String {
        return "That is my dream job!"
    }
}

class MyViewModel(val hobby: Hobby, val dreamJob: DreamJob) : ViewModel() {
    fun doSomething(): String {
        return "Something has done."
    }
}