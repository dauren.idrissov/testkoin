package kz.daurenidrissov.testkoin.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kz.daurenidrissov.testkoin.R
import kz.daurenidrissov.testkoin.model.MyViewModel
import kz.daurenidrissov.testkoin.model.Person
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.qualifier.named

class MainActivity : AppCompatActivity() {

    val daur by inject<Person>()

    val viewModel by viewModel<MyViewModel>()

    val sadPerson: Person by inject(named("sad"))
    val happyPerson: Person by inject(named("happy"))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val chen = get<Person>()
        mainTextView.text = chen.hobby.findHobby()

        secondaryTextView.text = daur.chill()

        accentTextView.text = callViewModel()

        lastTextView.text = "Sad Man: " + sadPerson.chill() +
                "\nHappy Man: " + happyPerson.chill()
    }

    fun callViewModel(): String {
        return viewModel.doSomething()
    }
}
