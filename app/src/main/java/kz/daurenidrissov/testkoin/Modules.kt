package kz.daurenidrissov.testkoin

import kz.daurenidrissov.testkoin.model.*
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val appModule = module {
    // defs a singleton
    single { DreamJob() }

    single<Person>(named("sad")) {
        Person2(
            get(),
            get()
        )
    }
    single<Person>(named("happy")) {
        Person(
            get(),
            get()
        )
    }

    // creates new instance every time
    factory { Hobby() }
    factory { Person(get(), get()) }
}

val viewModelModule = module {
    viewModel { MyViewModel(get(), get()) }
}